import {BrowserRouter,Switch, Route} from "react-router-dom";
import './App.css';
import About from "./containers/About/About";
import Contact from "./containers/Contact/Contact";
import Header from "./components/Header/Header";
import AddPostForm from "./containers/AddPostForm/AddPostForm";
import Home from "./containers/Home/Home";
import ReadMore from "./containers/ReadMore/ReadMore";


const App = () => {
  return (
      <div className="App">
          <BrowserRouter>
              <Header/>
              <Switch>
                 <Route path='/' exact component={Home} />
                 <Route path='/add' component= {AddPostForm} />
                  <Route path ='/posts/:id' component={ReadMore}/>
                 <Route path='/about'  component={About} />
                 <Route path='/contact' component={Contact} />

              </Switch>
          </BrowserRouter>


      </div>
  );
};

export default App;
