import React, {useEffect, useState} from 'react';
import './ReadMore.css';
import ShowPost from "../../components/ShowPost/ShowPost";
import axios from "axios";

const ReadMore = ({match}) => {

    const [posts, setPosts] = useState([]);

    useEffect(()=>{
        const fetchData = async ()=>{

            const response = await  axios.get('https://blog-93444-default-rtdb.firebaseio.com/posts/'+match.params);
            const result = response.data;
            console.log(response)

        }
        fetchData().catch(console.error);

    }, [match.params])

    return (
        <div>
            <p>{match.params}</p>
        </div>
    );
};

export default ReadMore;