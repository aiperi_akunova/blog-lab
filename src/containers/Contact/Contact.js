import React from 'react';
import './Contact.css';

const Contact = () => {
    return (
        <div className='contact'>
            <div className='contact-inner'>
                <h3>Our contact details:</h3>
                <p><b>Phone number: </b>+996 555 41 33 32</p>
                <p><b>Email: </b>myblog@gmail.com</p>
                <p><b>Address: </b>Baker str. 77</p>
            </div>
        </div>
    );
};

export default Contact;