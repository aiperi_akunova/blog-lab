import React from 'react';
import './About.css';

const About = () => {
    return (
        <div className='about'>
            <h1>About our app!</h1>
            <div className='boxes'>
                <div className="box">
                    <h3>Developers</h3>
                    <p>
                        Developers are seeing more value in larger screens, and the benefits of doing more with a single
                        device. Apps designed for large screen devices increase those benefits even further.
                    </p>

                </div>
                <div className="box">
                    <h3>History</h3>
                    <p>
                        History of making seeing more value in larger screens, and the benefits of doing more with a single
                        device. Apps designed for large screen devices increase those benefits even further.
                    </p>
                </div>
                <div className="box">
                    <h3>Vision</h3>
                    <p>
                        Our vision are seeing more value in larger screens, and the benefits of doing more with a single
                        device. Apps designed for large screen devices increase those benefits even further.
                    </p>
                </div>
            </div>

        </div>
    );

};

export default About;