import React, {useState} from 'react';
import './AddPostForm.css';
import dayjs from "dayjs";
import axios from "axios";


const AddPostForm = ({history}) => {
    const [postToAdd, setPostToAdd] = useState({
        title: '',
        text: '',
        postDate: ''
    })


    const data ={
        'title': postToAdd.title,
        'text' :postToAdd.text,
        'time':  dayjs().format('DD.MM.YYYY HH.mm')
    }

    const toPostMessage = (e)=> {
        e.preventDefault();
        const fetchData = async () => {
            const response = await axios.post('https://blog-93444-default-rtdb.firebaseio.com/posts.json', data);
            history.push('/');
        }
        fetchData().catch(e => console.error(e));
    }

    const onInputChange = (e)=>{
        const {name,value} = e.target;
        setPostToAdd(prev=>({
            ...prev,
            [name]: value
        }))
    }

    return (
        <div className='addPost-box'>
            <h3>Add your post</h3>
            <form onSubmit={toPostMessage}>
                <input
                    className='title-post'
                    name = 'title'
                    value={postToAdd.title}
                    placeholder='Title'
                    onChange={onInputChange}
                    autoComplete='off'
                />
                <textarea
                    className='text-post'
                    name = 'text'
                    value={postToAdd.text}
                    placeholder='Post text'
                    onChange={onInputChange}
                    autoComplete='off'
                />
                <button className='post-btn'>Post</button>
            </form>
        </div>
    );
};

export default AddPostForm;