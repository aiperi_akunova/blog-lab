import React from 'react';
import './Posts.css';
import {NavLink} from "react-router-dom";

const Posts = props => {
    const path = 'posts/'+props.id;
    return (
        <div className='post'>
          <p><b>Created on:  </b>{props.time}</p>
            <h3>{props.title}</h3>
            <NavLink
                className='read-more-btn'
                to={path}

            >Read more >> </NavLink>

        </div>
    );
};

export default Posts;