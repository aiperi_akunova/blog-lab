import React from 'react';
import './ShowPost.css';

const ShowPost = props => {
    return (
        <div className='one-post'>
            <div className='post-info'>
                <p><b>Created on:  </b>{props.time}</p>
                <h3>{props.title}</h3>
                <p>{props.text}</p>
            </div>
            <div className='buttons'>
                <button className='edit-btn'>Edit </button>
                <button className='delete-btn'>Delete </button>
            </div>
        </div>
    );
};

export default ShowPost;