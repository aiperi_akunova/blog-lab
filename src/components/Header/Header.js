import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <div>
            <header>
                <div>
                    <h2>My blog</h2>
                </div>
                <div className='header-links'>
                    <NavLink  to='/'>Home</NavLink>
                    <NavLink  to='/add'>Add</NavLink>
                    <NavLink  to='/about'>About</NavLink>
                    <NavLink  to='/contact'>Contacts</NavLink>
                </div>
            </header>
        </div>
    );
};

export default Header;